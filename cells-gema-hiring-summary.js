{
  const {
    html,
  } = Polymer;
  /**
    `<cells-gema-hiring-summary>` Description.

    Example:

    ```html
    <cells-gema-hiring-summary></cells-gema-hiring-summary>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-gema-hiring-summary | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsGemaHiringSummary extends Polymer.mixinBehaviors([ CellsBehaviors.i18nBehavior, ], Polymer.Element) {
    static get is() {
      return 'cells-gema-hiring-summary';
    }

    static get properties() {
      return {
        /*No olvides quitar current date*/
        currentDate: {
          type: Date 
        },
        /*Verifica que sea  number*/ 
        date: {
          type: Object,
          value: () => {}
        },
        language: {
          type: String
        },
        localCurrency: {
          type: String
        },
        /*No olvides quitar interval*/
        interval: {
          type: Number
        },
        loanAmount: {
          type: Object,
          value: () => {}
        },
        fixedPaymentAmount: {
          type: Object,
          value: () => {}
        },
        listAmountsComissions: {
          type: Array
        },
        totalToPay: {
          type: Object,
          value: () => {}
        },
        term: {
          type: String
        },
        checkingAccount: {
          type: Object,
          value: () => {}
        },
        contractNumber: {
          type: Number
        },
        initialPaymentDate: {
          type: Object,
          value: () => {}
        },
        finalPaymentDate: {
          type: Object,
          value: () => {}
        },
        items: {
          type: Array,
          value: ()=> [
            {
              label: 'Ver comprobante de contratación y contrato',
              id: 'transfer',
              icon: 'coronita:pdf'
            }, {
              label: 'Compartir',
              id: 'transfer',
              icon: 'coronita:share'
            }
          ]
        }
      };
    }

    static get template() {
      return html `
    <style include="cells-gema-hiring-summary-styles cells-gema-hiring-summary-shared-styles"></style>
    <slot></slot>
      
    <div>
      <div class="top-container">
        <div class="cell-status-bar"></div>
        <bbva-header-main accessibility-text-icon-right1="Close" icon-right1="coronita:close" text="Préstamo personal">
        </bbva-header-main>

        <div class="key_values title-hiring-success">
        <cells-key-values key="Contratación exitosa">
        </div>
        <div class="key_values date-hiring-success">
        <bbva-panel-process id="currentDate" date="[[date.date]]" date-format="[[date.format]]" language="[[language]]" class="space-zero-panel-process font-size-style-hiring-date"></bbva-panel-process>
        </div>

        <bbva-panel-process amount="[[loanAmount.value]]" currency-code="[[loanAmount.currencyCode]]" language="[[language]]" class="space-zero-panel-process">
          <p slot="before-large-text" class="font-size-hiring-loan">Préstamo contratado</p>
        </bbva-panel-process>

        <div class="key_values">
        <hr class="small-line">
        </div>

        <div class="key_values for_key">
          <cells-key-values key="Pago diario fijo">
        </div>
        <div class="key_values for_value">
          <cells-key-values local-currency="[[localCurrency]]" language="[[language]]" value='[[fixedPaymentAmount]]'>
        </div>


        <div class="lista_key_values">
          <ul class="cells-key-values-list">
            <dom-repeat items="[[listAmountsComissions]]">
              <template>
                <li class="key_values_pair">
                  <cells-key-values class="inline" key="[[item.key]]" local-currency="[[item.currency]]" language="[[language]]"
                    value="[[item.value]]"></cells-key-values>
                </li>
              </template>
            </dom-repeat>
          </ul>
        </div>
    
      <div class="sub-key-values lista_key_values key_values_p">
          <div class="font_weight_light">
            <cells-key-values key="Total a pagar">
          </div>
          <div class="font_weight_bold">
            <cells-key-values local-currency="[[localCurrency]]" language="[[language]]" value='[[totalToPay]]'>
          </div>
        </div>
      </div>
    
      <div class="container-bottom-container-image">
      </div>
    
      <div class="bottom-container">
        <div class="container-bottom-container-credit-account-charge">
          <cells-key-values key="Cuenta de abono y cargo" class="for_key_black">
        </div>
    
        <div class="container-bottom-container-checks-contract-keys bottom-container-for-key">
          <cells-key-values key="Cuenta de cheques">
        </div>
    
        <div class="container-bottom-container-checks-contract-keys bottom-container-for-key">
          <cells-product-item description="[[checkingAccount]]" local-currency="USD" hide-quantities>
          </cells-product-item>
        </div>
    
        <div
          class="container-bottom-container-checks-contract-keys bottom-container-for-key bottom-container-separation-small">
          <cells-key-values key="Número de contrato">
        </div>
    
        <div class="container-bottom-container-checks-contract-values bottom-container-for-value">
          <cells-key-values value="[[contractNumber]]">
        </div>
    
        <div class="bottom-container-key-value-inline-black bottom-container-separation-large">
          <cells-key-values class="inline" key="Paga en un plazo de" value="[[term]]"></cells-key-values>
        </div>
    
        <hr class="line-large bottom-container-separation-medium">
    
    
        <div class="sub-key-values lista_key_values_black key_values_p bottom-container-separation-medium">
          <div class="font_weight_light">
            <cells-key-values key="Fecha inicial de pago">
          </div>
          <div class="font_weight_bold">
            <cells-atom-date date="[[initialPaymentDate.date]]" format="[[initialPaymentDate.format]]" locale="[[initialPaymentDate.locale]]"></cells-atom-date>
          </div>
        </div>
    
        <div class="sub-key-values lista_key_values_black key_values_p bottom-container-separation-small">
          <div class="font_weight_light">
            <cells-key-values key="Fecha final de pago">
          </div>
          <div class="font_weight_bold">
            <cells-atom-date date="[[finalPaymentDate.date]]" format="[[finalPaymentDate.format]]" locale="[[finalPaymentDate.locale]]"></cells-atom-date>
          </div>
        </div>
    
        <div class="btn-icons bottom-container-separation-large">
          <cells-operations-list operations="[[items]]" grid-columns="2" limit="4" icon-size="24px" view-more
            on-view-more="customAction">
          </cells-operations-list>
        </div>
    
        <div class="key_values bottom-container-separation-x-medium btn-exit">
          <bbva-button-default text="Salir"></bbva-button-default>
        </div>
    
      </div>
    </div>               
      `;
    }

    connectedCallback() {
      super.connectedCallback();
      this.time();
    }

    time() {
      this.currentDate = Date.now();
      var dateToString = Date(Date.now()).toString();
      let f = parseInt(dateToString.substr(dateToString.lastIndexOf(':') + 1, 2));
      let currentSeconds = (60 - f) * 1000;
      setTimeout(() => this.time(), currentSeconds);
    }


  }

  customElements.define(CellsGemaHiringSummary.is, CellsGemaHiringSummary);
}